package com.prex.base.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prex.base.api.entity.SysOauthClientDetails;

/**
 * @Classname SysOauthClientDetailsService
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-05 11:42
 * @Version 1.0
 */
public interface SysOauthClientDetailsService extends IService<SysOauthClientDetails> {
}
